# Add Cluster to ArgoCD

Add ArgoCD operator to a new OCP Cluster.

## Role Variables

| Variable                      | Required | Default | Choices    | Comments                                                            |
| ----------------------------- | -------- | ------- | ---------- | ------------------------------------------------------------------- |
| cluster_name                  | yes      | -       | -          | -                                                                   |
| base_domain                   | yes      | -       | -          | -                                                                   |
| ssl_validate                  | no       | true    | true/false | -                                                                   |
| argocd_server                 | yes      | -       | -          | -                                                                   |
| argocd_token                  | yes      | -       | -          | This is the API token that is retrievable by API call. See [here](https://argoproj.github.io/argo-cd/developer-guide/api-docs/#authorization) |
| api_cluster_argo | yes       | "{{cluster_name}}"   | - | The cluster argo is on |
| openshift_custom_certificates | no       | false   | true/false |  

